/**
 * 2015. 5. 28.
 */
package kr.co.hbilab.app;

/**
 * @author user
 *
 */
public interface Message {
    public void printMsg();
    public void printThrowWxception();
}
