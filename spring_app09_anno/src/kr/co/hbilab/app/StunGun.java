package kr.co.hbilab.app;

public class StunGun implements Weapon{
    int bettery;
    
    public StunGun(int bettery){
        this.bettery = bettery;
    }
    
    public void reload(){
        this.bettery = 100;
        System.out.println("Reload ("+bettery+")");
    }
    
    @Override
    public void use() {
        if(bettery>30){
            bettery-=30;
            System.out.println("지지직~ ("+bettery+")");
        }else{
            reload();
        }
    }

    @Override
    public void reuse() {
        
    }

    @Override
    public void drop() {
        
    }
    
}
