package spring_app11_anno;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// xml로 설정할수도 있지만 
// java객체로 spring bean에 대한 설정정보를 갖게 할수 있다
// @Configuration : java bean에 대한 설정정보를 갖는 annotation


@Configuration
public class conf {

    
    // <bean id="sender" class="spring_app10_anno.Sender"></bean> 코드를 자바로 표현하면 아래와같다
    @Bean
    public Sender sender(){
        return new Sender();
    }
    
    @Bean(name="monitor") //별칭가능
    public SystemMonitor systemMonitor(){
        SystemMonitor sm = new SystemMonitor();
        sm.setSender(new Sender());
        return sm;
    }
}
